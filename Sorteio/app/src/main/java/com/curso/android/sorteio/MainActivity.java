package com.curso.android.sorteio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exibirResultado(View v){
        TextView exibirResultado = findViewById(R.id.exibirResultado);
        int numero = new Random().nextInt(11);

        exibirResultado.setText("O Número Sorteado é : " +numero);

    }
}